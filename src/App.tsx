import * as React from "react";
import { GameInterface } from "./gui/GameInterface";
import { Board, Match } from "./lib/Board";
import { GameLog } from "./lib/GameLog";
import { End } from "./lib/GameStates/End";
import { GameState } from "./lib/GameStates/GameState";
import { OTurn } from "./lib/GameStates/OTurn";
import { Splash } from "./lib/GameStates/Splash";
import { XTurn } from "./lib/GameStates/XTurn";


interface Props {

}

interface State {
  gameState:GameState,
  gameSettings:GameSettings,
  board:Board,
  gameLog:GameLog
}

export interface GameSettings {
  boardWidth:number,
  boardHeight:number,
  minMatchLength:number
}

export class App extends React.Component<Props,State> {
  public splashState:GameState;
  public xTurnState:GameState;
  public oTurnState:GameState;
  public endState:GameState;

  constructor(props) {
    super(props);

    this.splashState = new Splash(this);
    this.xTurnState = new XTurn(this);
    this.oTurnState = new OTurn(this);
    this.endState = new End(this);

    const gameLog = new GameLog();

    this.state = {
      gameState: this.xTurnState,
      gameSettings: {
        boardWidth: 3,
        boardHeight: 3,
        minMatchLength: 3
      },
      board: null,
      gameLog
    }
  }

  public componentDidMount = ():void => {
    this.resetGame();
  }

  public resetGame = async ():Promise<void> => {
    await this.resetBoard();
    this.endState = new End(this);
    this.state.gameLog.clear();
    this.setGameState(this.splashState);
  }

  public resetBoard = async ():Promise<void> => {
    const { gameSettings } = this.state;
    const { boardWidth, boardHeight } = gameSettings;

    const board = new Board(boardWidth, boardHeight);
    board.createFields();

    return new Promise((resolve, reject) => {
      this.setState({
        board
      }, () => {
        resolve();
      })
    })
  }

  public setGameState = async (gameState:GameState):Promise<void> => {
    const { gameLog } = this.state;
    if (gameState.entryMessage) gameLog.log(gameState.entryMessage);

    return new Promise((resolve, reject) => {
      this.setState({
        gameState
      }, () => {
        resolve();
      })
    })
  }

  public checkForMatches = ():Match => {
    const { board, gameSettings } = this.state;
    const match = board.checkBoardForMatches(gameSettings.minMatchLength);

    if (match) {
      this.endState.entryMessage = `${match.firstField.state} wins!`;
    }

    return match;
  }

  public checkForDraw = ():boolean => {
    const { board } = this.state;
    return board.checkForDraw();
  }

  public logMessage = (message:string) => {
    const { gameLog } = this.state;
    gameLog.log(message);
    this.setState({
      gameLog
    })
  }

  public changeBoardWidth = (boardWidth:number) => {
    const { gameSettings } = this.state;
    gameSettings.boardWidth = Math.max(boardWidth, 3);
    gameSettings.boardWidth = Math.min(gameSettings.boardWidth, 9);
    gameSettings.minMatchLength = Math.min(gameSettings.minMatchLength, Math.max(gameSettings.boardWidth, gameSettings.boardHeight));
    this.setState({
      gameSettings
    }, () => {
      this.resetBoard();
    })
  }

  public changeBoardHeight = (boardHeight:number) => {
    const { gameSettings } = this.state;
    gameSettings.boardHeight = Math.max(boardHeight, 3);
    gameSettings.boardHeight = Math.min(gameSettings.boardHeight, 9);
    gameSettings.minMatchLength = Math.min(gameSettings.minMatchLength, Math.max(gameSettings.boardWidth, gameSettings.boardHeight));
    this.setState({
      gameSettings
    }, () => {
      this.resetBoard();
    })
  }

  public changeMinMatchLength = (minMatchLength:number) => {
    const { gameSettings } = this.state;
    gameSettings.minMatchLength = Math.max(minMatchLength, 3);
    gameSettings.minMatchLength = Math.min(gameSettings.minMatchLength, Math.max(gameSettings.boardWidth, gameSettings.boardHeight));
    this.setState({
      gameSettings
    })
  }

  public onStartButtonClick = () => {
    this.setGameState(this.xTurnState);
  }

  public onResetButtonClick = async (keepSettings:boolean) => {
    await this.resetGame();
    if (keepSettings) this.onStartButtonClick();
  }

  public render = () => {
    const { gameState, gameSettings, board, gameLog } = this.state;

    return (
      <GameInterface
        gameStateName={gameState.name}
        gameSettings={gameSettings}
        board={board}
        gameLog={gameLog}
        onFieldClick={gameState.onFieldClick}
        changeBoardWidth={this.changeBoardWidth}
        changeBoardHeight={this.changeBoardHeight}
        changeMinMatchLength={this.changeMinMatchLength}
        onStartButtonClick={this.onStartButtonClick}
        onResetButtonClick={this.onResetButtonClick}
      />
    )
  }
}