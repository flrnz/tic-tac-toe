interface FieldNeighbors {
  topRight:Field,
  right:Field,
  bottomRight:Field,
  bottom:Field
}

export class Field {
  private _id:number;
  public state:"X"|"O"|"empty";
  public neighbors:FieldNeighbors

  get id() { return this._id };

  constructor(id:number) {
    this._id = id;
    this.state = "empty";
    this.neighbors = {
      topRight: null,
      right: null,
      bottomRight: null,
      bottom: null
    }
  }
}