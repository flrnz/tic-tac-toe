import { App } from "../../App";
import { GameState, GameStateName } from "./GameState";
import { Field } from "../Field";


export class Splash implements GameState {
  private _app:App;
  public readonly name = GameStateName.Splash;
  public readonly entryMessage = "Tic-Tac-Toe";

  constructor(app:App) {
    this._app = app;
  }

  public onFieldClick = (field:Field) => {
    return;
  }
}