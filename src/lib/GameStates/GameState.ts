import { Field } from "../Field";

export enum GameStateName {
  Splash = "Splash",
  XTurn = "XTurn",
  OTurn = "OTurn",
  End = "End"
}

export interface GameState {
  name:GameStateName,
  entryMessage:string|null,
  onFieldClick:(field:Field)=>void
}