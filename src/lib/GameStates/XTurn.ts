import { App } from "../../App";
import { GameState, GameStateName } from "./GameState";
import { Field } from "../Field";


export class XTurn implements GameState {
  private _app:App;
  public readonly name = GameStateName.XTurn;
  public readonly entryMessage = "X to move!";

  constructor(app:App) {
    this._app = app;
  }

  public onFieldClick = (field:Field) => {
    const app = this._app;
    const { oTurnState, endState } = app;

    if (field.state !== "empty") {
      app.logMessage(`X to move - please pick an empty field!`);
      return;
    }

    field.state = "X";

    const match = app.checkForMatches();
    const draw = app.checkForDraw();
    let newGamestate:GameState = oTurnState;
    if (match || draw) newGamestate = endState;

    this._app.setGameState(newGamestate);
  }
}