import { App } from "../../App";
import { GameState, GameStateName } from "./GameState";
import { Field } from "../Field";


export class OTurn implements GameState {
  private _app:App;
  public readonly name = GameStateName.OTurn;
  public readonly entryMessage = "O to move!";

  constructor(app:App) {
    this._app = app;
  }

  public onFieldClick = (field:Field) => {
    const app = this._app;
    const { xTurnState, endState } = app;

    if (field.state !== "empty") {
      app.logMessage(`O to move - please pick an empty field!`);
      return;
    }

    field.state = "O";

    const match = app.checkForMatches();
    const draw = app.checkForDraw();
    let newGamestate:GameState = xTurnState;
    if (match || draw) newGamestate = endState;

    this._app.setGameState(newGamestate);
  }
}