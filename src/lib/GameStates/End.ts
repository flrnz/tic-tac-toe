import { App } from "../../App";
import { GameState, GameStateName } from "./GameState";
import { Field } from "../Field";


export class End implements GameState {
  private _app:App;
  public readonly name = GameStateName.End;
  public entryMessage = "Draw!";

  constructor(app:App) {
    this._app = app;
  }

  public onFieldClick = (field:Field) => {
    return;
  }
}