


export class GameLog {
  public messages:string[];

  public get lastMessage():string { return this.messages[this.messages.length-1] };

  constructor() {
    this.messages = [];
  }

  public log = (message:string) => {
    this.messages.push(message);
  }

  public clear = () => {
    this.messages = [];
  }
}