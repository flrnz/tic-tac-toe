import { Field } from "./Field";


export enum Direction {
  TOPRIGHT = "topRight",
  RIGHT = "right",
  BOTTOMRIGHT = "bottomRight",
  BOTTOM = "bottom"
}

export interface Match {
  firstField:Field,
  direction:Direction,
  length:number
}

export class Board {
  public fields:Field[] = [];
  private _width:number;
  private _height:number;

  get width() { return this._width };
  get height() { return this._height };

  constructor(width:number = 3, height:number = 3) {
    this._width = width;
    this._height = height;
  }

  public createFields = () => {
    for (let i = 1; i <= this._width*this._height; i++) {
      this.fields.push(new Field(i));
    }

    //Set neighbors
    this.fields.forEach((field, ind) => {
      //Top Right
      if (field.id%this._width !== 0 && field.id > this._width) {
        field.neighbors.topRight = this.fields[ind-this._width+1];
      } else {
        field.neighbors.topRight = null;
      }

      //Right
      if (field.id%this._width !== 0) {
        field.neighbors.right = this.fields[ind+1];
      } else {
        field.neighbors.right = null;
      }

      //Bottom Right
      if (field.id%this._width !== 0 && (this._width * this._height) - field.id >= this._width) {
        field.neighbors.bottomRight = this.fields[ind+this._width+1];
      } else {
        field.neighbors.bottomRight = null;
      }

      //Bottom
      if ((this._width * this._height) - field.id >= this._width) {
        field.neighbors.bottom = this.fields[ind+this._width];
      } else {
        field.neighbors.bottom = null;
      }
    })

    console.log("createFields() finished!", this);
  }

  public checkBoardForMatches = (minMatchLength:number):Match => {
    let match = null;

    for (let field of this.fields) {
      match = this.checkFieldForMatches(minMatchLength, field);
      if (match) break;
    }

    return match;
  }

  public checkForDraw = ():boolean => {
    return this.fields.every((field) => field.state !== "empty");
  }

  //We currently don't handle the edge case of multiple matches, but I don't think we really need to -FL
  public checkFieldForMatches = (minMatchLength:number, field:Field):Match => {
    let matchCount:number = 0;

    const getMatchingNeighborCount = (field:Field, direction:Direction):number => {
      if (field.state === "empty") return 0;
      let matchCounter = 1;
      while (field.neighbors[direction] && field.neighbors[direction].state === field.state) {
        matchCounter++;
        field = field.neighbors[direction];
      }
      return (matchCounter);
    }

    const generateReturnValue = (direction:Direction, length:number):Match => {
      return {
        firstField: field,
        direction,
        length
      }
    }

    //Diagonal (up)
    matchCount = getMatchingNeighborCount(field, Direction.TOPRIGHT);
    if (matchCount >= minMatchLength) {
      return generateReturnValue(Direction.TOPRIGHT, matchCount);
    }

    //Horizontal
    matchCount = getMatchingNeighborCount(field, Direction.RIGHT);
    if (matchCount >= minMatchLength) {
      return generateReturnValue(Direction.RIGHT, matchCount);
    }
    //Diagonal (down)
    matchCount = getMatchingNeighborCount(field, Direction.BOTTOMRIGHT);
    if (matchCount >= minMatchLength) {
      return generateReturnValue(Direction.BOTTOMRIGHT, matchCount);
    }

    //Vertical
    matchCount = getMatchingNeighborCount(field, Direction.BOTTOM);
    if (matchCount >= minMatchLength) {
      return generateReturnValue(Direction.BOTTOM, matchCount);
    }

    return null;
  }
}