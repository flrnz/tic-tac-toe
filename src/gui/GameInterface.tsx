import * as React from "react";
import { GameSettings } from "../App";
import { Board } from "../lib/Board";
import { Field } from "../lib/Field";
import { GameLog } from "../lib/GameLog";
import { GameStateName } from "../lib/GameStates/GameState";
import { BoardContainer, BoardField, Button, ButtonRow, GameInterfaceContainer, GameSettingsRow, MessageContainer } from "./GameInterface-styled";
import { Modal } from "./Modals";


interface Props {
  gameStateName:GameStateName,
  gameSettings:GameSettings,
  board:Board,
  gameLog:GameLog,
  onFieldClick:(field:Field)=>void,
  changeBoardWidth:(boardWidth:number)=>void,
  changeBoardHeight:(boardHeight:number)=>void,
  changeMinMatchLength:(minMatchLength:number)=>void,
  onStartButtonClick:()=>void,
  onResetButtonClick:(keepSettings:boolean)=>void
}

interface State {
  boardContainerWidth:number,
  boardContainerHeight:number
}

export class GameInterface extends React.Component<Props,State> {
  private _boardContainerRef:React.RefObject<HTMLDivElement>;

  constructor(props) {
    super(props)
    this._boardContainerRef = React.createRef();

    this.state = {
      boardContainerWidth: window.innerWidth,
      boardContainerHeight: window.innerHeight
    }
  }

  public componentDidMount = ():void => {
    window.addEventListener("resize", this.onResize);

    window.setTimeout(() => {
      this.onResize();
    })
  }

  public componentWillUnmount():void {
    window.removeEventListener("resize", this.onResize);
  }

  public onResize = () => {
    let boardContainerWidth = window.innerWidth;
    let boardContainerHeight = window.innerHeight;

    if (this._boardContainerRef.current) {
      boardContainerWidth = this._boardContainerRef.current.clientWidth;
      boardContainerHeight = this._boardContainerRef.current.clientHeight;
    }

    this.setState({
      boardContainerWidth,
      boardContainerHeight
    })
  }

  public getFieldClasses = (field:Field):string => {
    const { board } = this.props;

    let classNames = "board-field";
    if (field.id <= board.width) classNames += " border-top";
    if (!field.neighbors.right) classNames += " border-right";
    if (!field.neighbors.bottom) classNames += " border-bottom";
    if (field.id%board.width==1) classNames += " border-left";

    return classNames;
  }

  public render = () => {
    const { gameStateName, gameSettings, board, gameLog, onFieldClick, changeBoardWidth, changeBoardHeight, changeMinMatchLength, onStartButtonClick, onResetButtonClick } = this.props;
    const { boardContainerWidth, boardContainerHeight } = this.state
    const { boardWidth, boardHeight, minMatchLength } = gameSettings;

    if (!board) return null;

    let fieldSize = Math.min(boardContainerWidth/ board.width, boardContainerHeight/ board.height);

    return (
      <GameInterfaceContainer>
        <MessageContainer>
          {(gameStateName === GameStateName.XTurn || gameStateName === GameStateName.OTurn) && gameLog.lastMessage}
        </MessageContainer>

        <BoardContainer className="board-container" ref={this._boardContainerRef}
          fieldsX={board.width}
          fieldsY={board.height}
          fieldSize={fieldSize}
        >
          {board.fields.map((field) => {
            return (
              <BoardField 
                key={field.id}
                className={this.getFieldClasses(field)}
                onClick={() => onFieldClick(field)}
              >
                {field.state !== "empty" ? field.state : ""}
              </BoardField>
            )
          })}
        </BoardContainer>

        <Modal
          title={gameLog.lastMessage}
          visible={gameStateName===GameStateName.Splash}
        >
          <GameSettingsRow>
            <div>Board width:</div>
            <input type={"number"}
              value={boardWidth}
              onChange={(e) => changeBoardWidth(Number(e.target.value))}
            />
          </GameSettingsRow>

          <GameSettingsRow>
            <div>Board height:</div>
            <input type={"number"}
              value={boardHeight}
              onChange={(e) => changeBoardHeight(Number(e.target.value))}
            />
          </GameSettingsRow>

          <GameSettingsRow>
            <div>Minimum match length:</div>
            <input type={"number"}
              value={minMatchLength}
              onChange={(e) => changeMinMatchLength(Number(e.target.value))}
            />
          </GameSettingsRow>

          <ButtonRow>
            <Button onClick={onStartButtonClick}>Start</Button>
          </ButtonRow>
        </Modal>

        <Modal
          title={gameLog.lastMessage}
          visible={gameStateName===GameStateName.End}
        >
          <ButtonRow>
            <Button onClick={() => onResetButtonClick(false)}>Restart</Button>
          </ButtonRow>
          <ButtonRow>
            <Button onClick={() => onResetButtonClick(true)}>Restart (keep settings)</Button>
          </ButtonRow>
        </Modal>
      </GameInterfaceContainer>
    )
  }
}