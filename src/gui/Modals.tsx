import * as React from "react";
import { Curtain, ModalContainer, ModalContent, ModalHeader } from "./Modal-styled";


interface Props {
  title:string,
  visible:boolean
}

interface State {
  
  minimized:boolean
}

export class Modal extends React.Component<Props,State> {
  constructor(props) {
    super(props)
  }

  public render = () => {
    const { visible, title, children } = this.props;

    if (!visible) return null;

    return (
      <Curtain>
        <ModalContainer>
          <ModalHeader>{title}</ModalHeader>
          <ModalContent>
            {children}
          </ModalContent>
        </ModalContainer>
      </Curtain>
    )
  }

}