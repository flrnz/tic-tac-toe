import styled, {css} from "styled-components"



export const BoardField = styled.div`
  border: 1px solid black;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;

  user-select: none;
  cursor: pointer;

  &.border-top {
    border-top: 2px solid black;
  }

  &.border-right {
    border-right: 2px solid black;
  }

  &.border-bottom {
    border-bottom: 2px solid black;
  }

  &.border-left {
    border-left: 2px solid black;
  }
`;

export const BoardContainer = styled.div<{fieldsX:number, fieldsY:number, fieldSize:number}>`
  width: 80%;
  height: 80%;

  display: grid;
  grid-template-columns: ${(props) => `repeat(${props.fieldsX}, ${props.fieldSize}px)`};
  align-content: center;
  justify-content: center;
  box-sizing: border-box;

  font-family: sans-serif;
  font-size: ${(props) => `${props.fieldSize * 0.9 }px`};

  & > ${BoardField} {
    min-width: ${(props) => `${props.fieldSize}px`};
    min-height: ${(props) => `${props.fieldSize}px`};
    max-width: ${(props) => `${props.fieldSize}px`};
    max-height: ${(props) => `${props.fieldSize}px`};
  }
`;

export const GameInterfaceContainer = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: white;

  background-image: url("./public/bg.jpg");
`;

export const MessageContainer = styled.div`
  position: fixed;
  top: 0px;
  left: 0px;
  width: 100vw;
  height: 10vh;

  display: flex;
  justify-content: center;
  align-items: center;
  text-align: center;

  font-family: sans-serif;
  font-size: 48px;
  font-weight: bold;
  padding-bottom: 20px;

  @media (max-width: 900px) {
    font-size: 32px;
  }
`;


export const GameSettingsRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  & > input {
    max-width: 3em;
    text-align: right;
    margin-left: 5px;
  }

  padding-top: 10px;
`;

export const ButtonRow = styled.div`
  padding-top: 10px;
`;

export const Button = styled.div`
  width: 100%;
  border: 1px solid black;
  padding: 6px 5px 3px 5px;
  box-sizing: border-box;
  border-radius: 10px;
  text-align: center;
  user-select: none;
  cursor: pointer;
`;