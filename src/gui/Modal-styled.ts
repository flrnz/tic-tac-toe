import styled, {css} from "styled-components"



export const Curtain = styled.div`
  position: fixed;

  display: flex;
  align-items: center;
  justify-content: center;

  top: 0px;
  left: 0px;
  width: 100vw;
  height: 100vh;
  overflow: hidden;

  background-color: rgba(0, 0, 0, 0.8);
  z-index: 3;
`;

export const ModalContainer = styled.div`
  max-width: 80vw;
  max-height: 80vh;

  background-color: white;

  font-family: sans-serif;

  border-radius: 10px;
`;

export const ModalHeader = styled.div`
  font-size: 32px;
  font-weight: bold;

  padding-left: 20px;
  padding-right: 20px;

  padding-top: 10px;
  padding-bottom: 10px;

  border-bottom: 1px solid black;
`;

export const ModalContent = styled.div`
  padding-left: 20px;
  padding-right: 20px;
  padding-bottom: 10px;
`;