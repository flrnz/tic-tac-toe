const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

'use strict';

module.exports = (env, args) => {
  const entrypoint = 'index.tsx'
  const port = 3000;

  return({
    entry: ["babel-polyfill", path.resolve('src', entrypoint)],

    module: {
      rules: [
        //Javascript
        {
          test: /\.(js|jsx|ts|tsx)$/,
          exclude: /node_modules/,
          use: ['babel-loader']
        },

        //HTML
        {
          test: /\.html$/,
          use: [
            {
              loader: "html-loader"
            }
          ]
        },

        //CSS
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },

        //Fonts
        {
          test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'fonts/'
              }
            }
          ]
        },

        //Shaders
        {
          test: /\.glsl$/i,
          use: [
            {
              loader: 'raw-loader',
              options: {
                esModule: false,
              },
            },
          ],
        },
      ]
    },

    resolve: {
      extensions: ['*', '.js', '.jsx', '.ts', '.tsx']
    },

    output: {
      path: path.resolve(__dirname, '../dist'),
      publicPath: '/',
      filename: 'bundle.js'
    },

    plugins: [
      new HtmlWebPackPlugin({
        template: path.resolve('src', 'index.html'),
        publicPath: "./"
      }),
      new CopyPlugin({
        patterns: [
          {
            from: "public",
            to: "public"
          }
        ]
      }),
      // new BundleAnalyzerPlugin()
    ],

    devServer: {
      static: path.resolve(__dirname, '../dist'),
      port: port,
      hot: true
    }
  });
};